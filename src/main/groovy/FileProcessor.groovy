class FileProcessor {

    public static void main(String[] args) {
        def folder = new File(args[0])
        processFilesWithLeadingA(folder)
        processFilesWithLeadingOne(folder)
        processFilesWithLeadingD(folder)
    }

    static def processFilesWithLeadingA(File folder) {
        def filesWithLeadingA = folder.listFiles(
                {dir, file -> file ==~ /a_.*/ } as FilenameFilter
        )

        filesWithLeadingA.each { file ->
            file << "\n${ file.readLines().collect{ line -> line.count("a") }.sum() ?: 0 }"
        }
    }

    static def processFilesWithLeadingOne(File folder) {
        def filesWithLeadingOne = folder.listFiles(
                {dir, file -> file ==~ /1_.*/ } as FilenameFilter
        )

        filesWithLeadingOne.each { file ->
            def w = file.newWriter()
            int[][] matrix = MatrixUtil.generateRandom4x4Matrix()
            matrix.each {
                w.writeLine("${it.toList().join(" ")}")
            }
            w.writeLine("Diagonal: ${MatrixUtil.diagonalSum(matrix)}")
            w.writeLine("Antidiagonal: ${MatrixUtil.antidiagonalSum(matrix)}")
            w.close()
        }
    }

    static def processFilesWithLeadingD(File folder) {
        def filesWithLeadingD = folder.listFiles(
                {dir, file -> file ==~ /d_.*/ } as FilenameFilter
        )

        filesWithLeadingD.each { file ->
            def w = file.newWriter()
            w << "${ new Date().format("yyyy-MM-dd HH-mm-ss") }"
            w.close()
        }
    }

}
