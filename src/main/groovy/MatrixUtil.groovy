class MatrixUtil {

    static def diagonalSum(int[][] matrix) {
        def sum = 0
        (0..matrix.length - 1).each { int i ->
            sum += matrix[i][i]
        }
        sum
    }

    static def antidiagonalSum(int[][] matrix) {
        def sum = 0
        (0..matrix.length - 1).each { int i ->
            sum += matrix[i][matrix.length - 1 - i]
        }
        sum
    }

    static int[][] generateRandom4x4Matrix() {
        int[][] matrix = new int[4][4]
        def rand = new Random()
        4.times { i ->
            4.times { j ->
               matrix[i][j] = rand.nextInt(10)
            }
        }
        matrix
    }

}
