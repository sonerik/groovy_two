import spock.lang.Specification

class FileProcessorTests extends Specification {

    def "count letter a in file that begins with 'a_'"() {
        def testFolder = new File(getClass().getResource("/a_test").getFile())

        def testFile = testFolder.listFiles().first()
        testFile.withWriter { w -> w <<
                """${"a" * 5}
                  |${"a" * 10}
                  |${"ab" * 10}""".stripMargin()
        }

        FileProcessor.processFilesWithLeadingA(testFolder)
        def x = testFile.readLines().last().toInteger()
        expect:
        x == 25
    }

    def "generate matrix 4x4 for file that begins with '1_'"() {
        def testFolder = new File(getClass().getResource("/1_test").getFile())

        def testFile = testFolder.listFiles().first()
        testFile.withWriter { w -> w << "" }

        FileProcessor.processFilesWithLeadingOne(testFolder)
        int[][] matrix = new int[4][4]

        def actualDiagonal = null
        def actualAntidiagonal = null

        testFile.withReader { r ->
            4.times { int i ->
                r.readLine().split().eachWithIndex { it, int j ->
                    matrix[i][j] = it.toInteger()
                }
            }
            def diagonalMatcher = r.readLine() =~ /(\d+)/
            actualDiagonal = diagonalMatcher[0][0] as Integer

            def antidiagonalMatcher = r.readLine() =~ /(\d+)/
            actualAntidiagonal = antidiagonalMatcher[0][0] as Integer
        }

//        matrix.each {
//            println(it.toList().join(" "))
//        }

        def expectedDiagonal = matrix[0][0] + matrix[1][1] + matrix[2][2] + matrix[3][3]
        def expectedAntidiagonal = matrix[0][3] + matrix[1][2] + matrix[2][1] + matrix[3][0]

        expect:
        actualDiagonal == expectedDiagonal
        actualAntidiagonal == expectedAntidiagonal
    }

    def "generate date in 'yyyy-MM-dd HH-mm-ss' format"() {
        def testFolder = new File(getClass().getResource("/d_test").getFile())

        def testFile = testFolder.listFiles().first()
        testFile.withWriter { w -> w << "" }

        def expected = new Date().format("yyyy-MM-dd HH-mm-ss")
        FileProcessor.processFilesWithLeadingD(testFolder)

        def actual = null
        testFile.withReader { r ->
            actual = r.readLine()
        }

        expect:
        actual == expected
    }

}
