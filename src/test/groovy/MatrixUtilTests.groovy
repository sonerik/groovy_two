import spock.lang.Specification

class MatrixUtilTests extends Specification {

    int[][] matrix = [
            [2, 1, 1, 3],
            [1, 2, 3, 1],
            [1, 3, 2, 1],
            [3, 1, 1, 2]
    ]

    def "matrix main diagonal sum"() {
        expect:
        MatrixUtil.diagonalSum(matrix) == 2 * 4
    }

    def "matrix antidiagonal sum"() {
        expect:
        MatrixUtil.antidiagonalSum(matrix) == 3 * 4
    }

}
